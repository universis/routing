# @universis/routing

[expressjs](https://github.com/expressjs/express) routing tools and middlewares

## Installation

    npm i @universis/routing

## Usage

### disableCache()

Disables caching by including `Cache-Control`, `Pragma` and `Expires` http headers

    import express from 'express';
    import { disableCache } from '@universis/routing';

    app = express();
    
    app.get('/hello', disableCache(), (req, res) => {
        return res.json({
            message: 'Hello World'
        });
    });

### remoteClientAccessControl(addresses, proxyAddressForwarding)

Restricts the access to the target based on a collection of remote client addresses.

> addresses 

An array of strings which represent the collection of remote clients that have access to that resource

> proxyAddressForwarding

A boolean which indicates whether the application is running behind a reverse proxy

    import express from 'express';
    import { remoteClientAccessControl } from '@universis/routing';

    app = express();
    
    app.get('/hello', remoteClientAccessControl([
        '127.0.0.1/32'
    ], false), (req, res) => {
        return res.json({
            message: 'Hello World'
        });
    });
