import { Handler } from "express";

export declare function remoteAddressAccessControl(addresses: string[], proxyAddressForwarding: boolean): Handler;