import { HttpForbiddenError, HttpBadRequestError } from '@themost/common';
import ipRangeCheck from 'ip-range-check';
/**
 * @param {Array<string>} addresses 
 * @param {boolean} proxyAddressForwarding 
 * @returns 
 */
function remoteAddressAccessControl(addresses, proxyAddressForwarding) {
    return function allowClientHandler(req, res, next) {
        const list = Array.isArray(addresses) ? addresses : [];
        if (list.length === 0) {
            return next(new HttpForbiddenError());
        }
        let remoteAddress = proxyAddressForwarding ? req.headers['x-forwarded-for'] : req.connection.remoteAddress;
        // handle multiple ip addresses
        let remoteAddresses = [];
        if (typeof remoteAddress === 'string') {
            remoteAddresses = remoteAddress.replace(/\s/g,'').split(',');
        }
        if (remoteAddresses.length === 0) {
            return next(new HttpBadRequestError('Invalid remote client'));
        }
        // get last remote address which is either a valid remote address without proxy
        // or the last proxy provided by x-forwarded-for header
        remoteAddress = remoteAddresses[remoteAddresses.length - 1];
        // validate remoteAddress
        if (ipRangeCheck(remoteAddress, list) === false) {
            return next(new HttpForbiddenError('Resource access is not allowed'));
        }
        return next();
    }
}

export {
    remoteAddressAccessControl
}

