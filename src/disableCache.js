import onHeaders from 'on-headers';
/**
 * 
 * @returns {import('express').Handler}
 */
function disableCache() {
    return (req, res, next) => {
        onHeaders(res, ()=> {
            res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
            res.setHeader('Pragma', 'no-cache');
            res.setHeader('Expires', 0);
            // remove express js etag header
            res.removeHeader('ETag');
        });
        return next();
    }
}

export {
    disableCache
}