import express from 'express';
import { disableCache } from '../src/disableCache';
import { remoteAddressAccessControl } from '../src/remoteAddressAccessControl';
import request from 'supertest';

describe('allowClient', () => {

    /**
     * @type {Express.Application}
     */
    let app;
    beforeAll(() => {
        app = express();
    });

    it('should validate remote client', async () => {
        app.get('/hello', disableCache(), remoteAddressAccessControl([
            '127.0.0.1/32'
        ], false), (req, res) => {
            return res.json({
                message: 'Hello World'
            });
        });

        app.get('/client', disableCache(), remoteAddressAccessControl([
            '192.168.0.0/24'
        ], false), (req, res) => {
            return res.json({
                message: 'Hello Client'
            });
        });
        let response = await request(app).get('/hello');
        expect(response.statusCode).toEqual(200);
        response = await request(app).get('/client');
        expect(response.statusCode).toEqual(403);

        app.get('/resource1', disableCache(), remoteAddressAccessControl([
            '127.0.0.1/32'
        ], true), (req, res) => {
            return res.json({
                message: 'Hello Client'
            });
        });
        response = await request(app).get('/resource1');
        expect(response.statusCode).toEqual(400);

    });

});