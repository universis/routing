import express from 'express';
import { disableCache } from '../src/disableCache';
import request from 'supertest';

describe('disableCache', () => {

    /**
     * @type {Express.Application}
     */
    let app;
    beforeAll(() => {
        app = express();
    });

    it('should disable cache headers', async () => {
        app.get('/hello', disableCache(), (req, res) => {
            return res.json({
                message: 'Hello World'
            });
        });
        const response = await request(app).get('/hello');
        expect(response.statusCode).toEqual(200);
        expect(response.get('pragma')).toEqual('no-cache');
        expect(response.get('expires')).toEqual('0');
    });

});